#! /usr/bin/env false

use v6.d;

use LogP6 :configure;
use Matrix::Bot::Plugin::AutoAcceptInvites;
use Matrix::Bot::Plugin;
use Matrix::Bot;

use Local::Sjon::InitiativeList;
use Local::Sjon::Social;

unit module Local::Sjon;

sub MAIN () is export
{
	# Configure logging
	filter(:name(''), :level($trace), :update);

	# Run the bot
	Matrix::Bot.new(
		home-server => %*ENV<MATRIX_HOMESERVER> // "https://matrix.org",
		username => %*ENV<MATRIX_USERNAME> // "sjon",
		password => %*ENV<MATRIX_PASSWORD> // Str,
		access-token => %*ENV<MATRIX_TOKEN> // Str,
		plugins => [
			Local::Sjon::InitiativeList.new,
			Local::Sjon::Social.new,
			Matrix::Bot::Plugin::AutoAcceptInvites.new,
		],
	).run;
}

=begin pod

=NAME    Local::Sjon
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
