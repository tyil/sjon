#! /usr/bin/env false

use v6.d;

use Matrix::Bot::Plugin;

unit class Local::Sjon::InitiativeList is Matrix::Bot::Plugin;

has Int %.initiatives;
has Int $.current;
has Str @.order;

has Str @.prefixes = [ ".i", ".init", ".initiative" ];

multi method handle-room-text ($e --> Str)
{
	self.log.trace("{$?CLASS.^name}.handle-room-text");

	my @words = $e.message.words;

	return Str unless @!prefixes ∋ @words[0];

	given @words[1].fc {
		when "set" { self!set(@words[2], @words[3]) }
		when "next" { self!next }
		when "clear" { self!clear }
		when "list" { self!show }
		default { self.log.warn("$_ is not a valid initiative command") }
	}
}

method !clear ()
{
	self.log.trace("{$?CLASS.^name}!clear");

	# Reset initiatives
	%!initiatives = %();
	$!current = Int;

	"Initiative list cleared";
}

method !next
{
	self.log.trace("{$?CLASS.^name}!next");

	if ($!current ~~ Int:U) {
		self.log.debug("Starting combat round, setting current to 0");

		$!current = 0;
	} else {
		$!current++;

		self.log.debug("Progressing through combat, set current to $!current");

		if ((@!order.elems - 1) < $!current) {
			self.log.debug("{@!order.elems - 1} <= $!current, setting current to 0");

			$!current = 0;
		}
	}

	"**{@!order[$!current]}**'s turn!";
}

method !set ($player, $roll)
{
	self.log.trace("{$?CLASS.^name}!set");
	self.log.info("Adding $player to initiative list with $roll");

	%!initiatives{$player} = $roll.Int;

	@!order = %!initiatives
		.sort(-*.value)
		.map(*.key)
		;

	"**{$player}**'s initiative set to **$roll**"
}

method !show ()
{
	self.log.trace("{$?CLASS.^name}!show");

	if (!%!initiatives) {
		self.log.debug("Initiative list is empty!");

		return q:to/EOF/
			The initiative list is still empty!
			Add players using `.initiative set <player> <roll>`.
			EOF
	}

	@!order.map({
		state $i++;

		$!current && ($i - 1) == $!current
			?? "$i. **$_** ({%!initiatives{$_}})"
			!! "$i. $_ ({%!initiatives{$_}})"
			;
	}).join("\n");
}

=begin pod

=NAME    Local::Sjon::InitiativeList
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
