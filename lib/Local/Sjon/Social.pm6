#! /usr/bin/env false

use v6.d;

use Matrix::Bot::Plugin;

unit class Local::Sjon::Social is Matrix::Bot::Plugin;

method handle-connect ($e)
{
	for $e.rooms -> $room {
		$e.bot.client.send($room, "Here's Sjonnie", :type<m.notice>);
	}
}

=begin pod

=NAME    Local::Sjon::Social
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
